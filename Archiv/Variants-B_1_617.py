import sys, os, time, csv
from datetime import datetime, timedelta
import numpy as np, matplotlib.pyplot as plt
from scipy import stats
from scipy import constants as const
from scipy.optimize import curve_fit
from scipy import interpolate as ip

#import urllib.request
#import glob
#from PIL import Image

## Data (Quelle: RKI-Variantenbericht, Kapitel 2, Tabelle 5)
old_data = {
    "07.07.2021": np.array([[16, 17, 18, 19, 20,  21,  22,   24,   25,   26],
                            [ 6, 28, 49, 61, 94, 225, 362, 1366, 1405, 1938]]),
    "30.06.2021": np.array([[16, 17, 18, 19, 20,  21,  22,   24,   25],
                            [ 6, 28, 49, 61, 94, 225, 362, 1366, 1400]]),
    "23.06.2021": np.array([[16, 17, 18, 19, 20,  21,  22,   24],
                            [ 6, 28, 49, 61, 94, 225, 377, 1295]])
            } #Die alten Daten sind nützlich, um die Unsicherheit der Zahlen (z.B. wegen Nachmeldungen) abzuschätzen
last_updated = "14.07.2021"
fitData = np.array([[16, 17, 18, 19, 20,  21,  22,   24,   25,   26,   27],
                    [ 6, 28, 49, 61, 94, 225, 362, 1366, 1405, 1944, 2833]])
label = ["Kalenderwoche", "Erfasste Fälle der Variantengruppe B.1.617"]

## Fit
def funcExp(x, a=5, b=1/60): #y = a * e^(x*b)
    return a*np.exp(x*b)
popt, pcov = curve_fit(funcExp, fitData[0], fitData[1])
print("Exp Fit (a, b):", popt)
r_factor = np.exp(popt[1])
Weeks_until_double = np.log(2)/np.log(r_factor)
print("Wöchentlicher R-Faktor:", r_factor)
if Weeks_until_double > 0:
    print("Verdopplung nach", Weeks_until_double, "Wochen")
else:
    print("Halbierung nach", -Weeks_until_half, "Wochen")

## Prediction
HerdImmunityFilePath = "HerdImmunityDate.txt"
with open(HerdImmunityFilePath, "r", encoding='utf-8') as inputFile:
    HerdImmunityString = inputFile.read()
HerdImmunityDate = datetime.strptime(HerdImmunityString, "%d.%m.%Y")
HerdImmunityCalendarWeek = HerdImmunityDate.isocalendar()[1]
print("Herd Immunity:", HerdImmunityString, "KW:", HerdImmunityCalendarWeek)

## Plot
SaveFigures = True
# Vorhersage
x_Values = np.arange(fitData[0][0]-0.2, HerdImmunityCalendarWeek + 0.7, 0.1)
plt.plot(x_Values, funcExp(x_Values, *popt), label='Exponential-Fit (Wochen-R-Wert = {:.2f})'.format(r_factor))
plt.scatter(fitData[0], fitData[1], label='Daten')
plt.vlines(HerdImmunityCalendarWeek, 0, funcExp(HerdImmunityCalendarWeek+0.7, *popt), label="Mögliche Herdenimmunität (KW " + str(HerdImmunityCalendarWeek) + ")", linestyle='dashed')

plt.subplots_adjust(left=0.14, bottom=0.11, right=0.99, top=0.94)
plt.title('Variantengruppe B.1.617 ('+ last_updated +')')
plt.grid(linestyle='--', alpha=0.5)
plt.xlabel('Kalenderwoche (2021)')
plt.ylabel('Erfasste Fälle (Sequenzierung und Varianten-PCR)')
plt.yscale('log')
plt.legend()

#plt.show()
if SaveFigures:
    figureFilename = 'Variants-B_1_617-Prediction.svg'
    plt.savefig(figureFilename)
    print('Saved Figure as: ' + figureFilename)
plt.close()


# Close up
x_Values = np.arange(fitData[0][0]-0.2, fitData[0][-1] + 0.7, 0.1)
plt.plot(x_Values, funcExp(x_Values, *popt), label='Exponential-Fit (Wochen-R-Wert = {:.2f})'.format(r_factor), color='black')
for n, date in enumerate(old_data):
    plt.scatter(old_data[date][0], old_data[date][1], label= 'Alte Daten ({})'.format(date), alpha=1.2/(n+2))
plt.scatter(fitData[0], fitData[1], label='Daten', color='black')

plt.subplots_adjust(left=0.14, bottom=0.11, right=0.99, top=0.94)
plt.title('Variantengruppe B.1.617 ('+ last_updated +')')
plt.grid(linestyle='--', alpha=0.5)
plt.xlabel('Kalenderwoche (2021)')
plt.ylabel('Erfasste Fälle (Sequenzierung und Varianten-PCR)')
plt.legend()

#plt.show()
if SaveFigures:
    figureFilename = 'Variants-B_1_617-Closeup.svg'
    plt.savefig(figureFilename)
    print('Saved Figure as: ' + figureFilename)
plt.close()
