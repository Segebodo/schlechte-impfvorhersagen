# Schlechte Impfvorhersagen

Diese Vorhersagen sollten nicht zu ernst genommen werden und sind wahrscheinlich weit neben der Wahrheit. Diese Plots können im besten Fall aktuelle Trends extrapolieren. Um eine aussagekräftige Vorhersage zu machen müssten die Produktions- und Lieferzusagen der HerstellerInnen sowie womöglich auftretende Schwierigkeiten oder Verbesserungen betrachtet werden. Hier werden jedoch lediglich die bisherigen Impfraten als Basis für die Vorhersage genommen.

"Immer wieder hieß es, dass 60 bis 70 Prozent der Bevölkerung sich impfen müssten, um die Ausbreitung des Virus zu stoppen. Schon als wir es mit der ansteckenderen Alpha-Variante von SARS-CoV-2 zu tun hatten, schätzten Expert*innen die Schwelle zur Herdenimmunität eher auf 80 oder 90 Prozent.

Mit der Delta-Variante als Dominante und wenn wir davon ausgehen, dass Geimpfte zumindest gelegentlich noch ansteckend sind, müssten sich wohl noch mehr Menschen impfen lassen. Nur dann wäre eine Herdenimmunität erreichbar. **Das RKI hält das Erreichen einer Herdenimmunität im Sinne einer Auslöschung des Virus daher nicht mehr für realistisch.**"[SWR-Artikel](https://www.swr.de/wissen/ansteckend-trotz-corona-impfung-forschung-100.html)

**Datenquellen**: [COVID-19 Impfdashboard](https://impfdashboard.de/daten), [Virusvariantenbericht des RKI](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/DESH/Berichte-VOC-tab.html)

![Vergleich sinnvoller Modelle mit den Daten](Sinnvolle Modelle current.png)

![Herden-Immunitäts Vorhersage](Schlechte Impfvorhersage current.png)

![Impfunge pro Tag Vorhersage](Schlechte Impfvorhersage pro Tag current.png)

![Vergleich weiterer Modelle mit den Daten](Vergleich mehrerer Modelle current.png)

## Bisheriger Verlauf der Schätzungen:

![Vergleich sinnvoller Modelle mit den Daten](Sinnvolle Modelle current_hindsight.gif)

![Herden-Immunitäts Vorhersage](Schlechte Impfvorhersage current_hindsight.gif)

![Impfunge pro Tag Vorhersage](Schlechte Impfvorhersage pro Tag current_hindsight.gif)

![Vergleich weiterer Modelle mit den Daten](Vergleich mehrerer Modelle current_hindsight.gif)
