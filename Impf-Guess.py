import sys, os, time, csv
from datetime import datetime, timedelta
import numpy as np, matplotlib.pyplot as plt
from scipy import stats
from scipy import constants as const
from scipy.optimize import curve_fit
from scipy import interpolate as ip

import urllib.request
import glob
from PIL import Image

url = "https://impfdashboard.de/static/data/germany_vaccinations_timeseries_v2.tsv"
fname = "germany_vaccinations_timeseries_v2.tsv"
downloadNewData = True
showCurrentFigures = False
saveFigure = True
modelComparisonPreviewLength = 3
estimationPreviewLength = 270
maximaleImpfrateIsrael = 0.015 # in Impdosen pro Bevölkerung pro Tag
hindsight_Days_Step = 14

impfQuote = 0.85
Gesamtbevoelkerung = 83.19

Sigma_Fit_Daten = 5 #Mio Dosen

needed_headers = ["date",
                  "personen_erst_kumulativ",
                  "personen_voll_kumulativ",
                  "personen_auffrisch_kumulativ"]

## Download data
if downloadNewData:
    print('Downloading new data from', url, ' ...')
    urllib.request.urlretrieve(url, fname)
    print('Saved data as:', fname)

## Read Data
def readData(filePath):
    with open(filePath, "r", encoding='utf-8') as inputFile:
        csvFile = csv.reader(inputFile, delimiter='\t', lineterminator='\n')
        firstRow = True
        data = {}
        header = next(csvFile)
        header_dict = {}
        for name in needed_headers:
            try:
                header_dict[header.index(name)] = name
            except ValueError:
                print("Fehler in den heruntergeladenen Daten: Die benötigte Spalte", name, "konnte nicht gefunden werden. Vielleicht wurde sie umbenannt.")
                raise
            data[name] = []
        needed_columns = list(header_dict.keys())
        for row in csvFile:
            for i,cell_String in enumerate(row):
                if i in needed_columns:
                    if header_dict[i] == "date":
                        cell_data = datetime.strptime(cell_String, "%Y-%m-%d")
                    else:
                        cell_data = int(cell_String)
                    data[header_dict[i]].append(cell_data)
    return data, header

data, header = readData(fname)
dates = data["date"]
data["date"] = np.array([(date - data["date"][-1]).days for date in data["date"]])

Boost_Geimpfte_Personen_in_Mio = np.array(data["personen_auffrisch_kumulativ"])*10**(-6)
Vollst_Geimpfte_Personen_in_Mio = np.array(data["personen_voll_kumulativ"])*10**(-6)
Vollst_Geimpfte_Personen_Sigma = np.array([Sigma_Fit_Daten]*len(data["personen_voll_kumulativ"]))
Erst_Geimpfte_Personen_in_Mio = np.array(data["personen_erst_kumulativ"])*10**(-6)


## Fit functions
def funcPolynomial(x, a, b, c): #y = a*x^2 + b*x + c
    return a*x**2 + b*x + c
def funcCubic(x, a, b, c, d): #y = a*x^3 + b*x^2 + c*x + d
    return a*x**3 + b*x**2 + c*x + d
def funcLinear (x, a, b): #y = a*x + b
    return a*x + b
def funcSCurve (x, a, b, c):
    return np.divide(a, 1+np.exp(b-c*x))
def funcSCurve_Ableitungen (x,a,b,c):
    return funcSCurve(x,a,b,c)*np.array([1/a,
                                          -np.divide(np.exp(b-c*x), 1+np.exp(b-c*x)),
                                          np.divide(x*np.exp(b-c*x), 1+np.exp(b-c*x))
                                          ])
def limitedGrowth(x, a, b, c, d):
    return a*(x+d) + b/c*np.exp(-c*x)
def limitedGrowthIsrael(x, b, c, d):
    a = maximaleImpfrateIsrael*Gesamtbevoelkerung
    return a*(x+d) + b/c*np.exp(-c*x)

## Functions
def clearTempFolder():
    filelist = [ f for f in os.listdir('temp') if f.endswith(".png") ]
    for f in filelist:
        #print(f)
        os.remove(os.path.join('temp', f))
# Plot helper
def clip(yData):
    return np.clip(yData,0, Gesamtbevoelkerung)

def plot_model(function, params, label):
    return plt.plot(modelComparisonPreviewDays, clip(function(modelComparisonPreviewDays, *params)), label=label, alpha=0.7, linewidth=2)
def plot_model_confidence(function, params, errors, color, signs=1):
    upper_params = params + np.asarray(signs)*errors
    lower_params = params - np.asarray(signs)*errors
    print()
    print(params)
    print(upper_params)
    print(lower_params)
    return plt.fill_between(modelComparisonPreviewDays, clip(function(modelComparisonPreviewDays, *upper_params)), clip(function(modelComparisonPreviewDays, *lower_params)), alpha=0.23, color=color)

def plot_estimate(function, params, label, alpha=1):
    return plt.plot(estimationPreviewDates, clip(function(estimationPreviewDays, *params)), label=label, alpha=alpha)
def plot_estimate_diff(function, params, label, alpha=1):
    return plt.plot(estimationPreviewDates[1:], np.diff(clip(function(estimationPreviewDays, *params))).clip(min=0), label=label, alpha=alpha)
def plot_estimate_confidence(function, ableitungen, params, covar, color):
    ableitungen_ = [ableitungen(day, *params) for day in estimationPreviewDays]
    std = np.array([  np.sqrt(np.dot(ableitungen_[day], np.dot(covar, ableitungen_[day])))  for day in estimationPreviewDays])
    function_ = function(estimationPreviewDays, *params)
    print(std)
    return plt.fill_between(estimationPreviewDates, clip(function_+std), clip(function_-std), alpha=0.23, color=color)

## 
for hindsight_Length in [0, (dates[-1] - datetime.strptime('2021-02-01', "%Y-%m-%d")).days]:
    isHindsight = not (hindsight_Length == 0)
    print(hindsight_Length, isHindsight)
    folderName = ('temp'+os.sep) if isHindsight else ('Archiv' +os.sep)
    if isHindsight:
        if not os.path.exists('temp'):
            os.makedirs('temp')
        else:
            clearTempFolder()
    for i in range(0, hindsight_Length+1, hindsight_Days_Step):

        n = len(data["date"]) -i #Only use data from n days ago
        hindsightSuffix = '_temp-{:0>4}'.format(n) if isHindsight else ''
        fitData = [data["date"][:n], Vollst_Geimpfte_Personen_in_Mio[:n]]
        fitData_std = Vollst_Geimpfte_Personen_Sigma[:n]
        fitDates = dates[:n]
        boost_fitData = [data["date"][:n], Boost_Geimpfte_Personen_in_Mio[:n]]
        erst_fitData = [data["date"][:n], Erst_Geimpfte_Personen_in_Mio[:n]]
        if isHindsight:
            print('i:', i)

        ## Fitting
        poptCub, pcovCub = curve_fit(funcCubic, fitData[0], fitData[1], sigma=fitData_std, p0=[3.72022865e-06, 2.71e-03, 9.80e-01, 8.94e+01])
        perrCub = np.sqrt(np.diag(pcovCub))
        poptPoly, pcovPoly = curve_fit(funcPolynomial, fitData[0], fitData[1], sigma=fitData_std, p0=[2.71e-03, 9.8e-01, 8.9e+01])
        perrPoly = np.sqrt(np.diag(pcovPoly))
        poptSCur, pcovSCur = curve_fit(funcSCurve, fitData[0], fitData[1], sigma=fitData_std, bounds=([0.01, -np.inf, 0], [250, np.inf, np.inf]), p0=[1.03777885e+02, -1.30824185e+0, 2.99848624e-02])
        perrSCur = np.sqrt(np.diag(pcovSCur))
        poptLimG, pcovLimG = curve_fit(limitedGrowth, fitData[0], fitData[1], sigma=fitData_std, bounds=([0.001,0.001,0.0001,1],np.inf) , p0=[1.4e-01, 8.5e-03, 3.9e-02, 4.5e+01], maxfev=1e5)
        perrLimG = np.sqrt(np.diag(pcovLimG))
        poptLimGI, pcovLimGI = curve_fit(limitedGrowthIsrael, fitData[0], fitData[1], sigma=fitData_std, bounds=([10**(-2),10**(-4),-800],[10**(2), 1, 800]), p0=[1.1e+00, 1.6e-03, -5e+02], maxfev=1e5)
        perrLimGI = np.sqrt(np.diag(pcovLimGI))
        poptLin, pcovLin = curve_fit(funcLinear, fitData[0], fitData[1], sigma=fitData_std, p0=[0.43, 71])
        poptLin_Recent, pcovLin_Recent = curve_fit(funcLinear, fitData[0][-8:], fitData[1][-8:], bounds=(0, np.inf), p0=[0.61, 85])
        print('Cubic Fit (a, b, c, d):', poptCub)
        print('Poly Fit (a, b, c):', poptPoly)
        #print(pcovPoly)
        #print('Poly rel Errors in % (a, b, c):', perrPoly/np.absolute(poptPoly)*100)
        print('S-Curve Fit (a, b, c):', poptSCur)
        #print(pcovSCur)
        #print('S-Curve rel Errors in % (a, b, c):', perrSCur/np.absolute(poptSCur)*100)
        print('Lim Growth Fit (a, b, c, d):', poptLimG)
        #print(pcovLimG)
        #print('Lim Growth rel Errors in % (a, b, c, d):', perrLimG/np.absolute(poptLimG)*100)
        print('Lim Growth Israel Fit (b, c, d):', poptLimGI)
        #print(pcovLimGI)
        #print('Lim Growth Israel rel Errors in % (b, c, d):', perrLimGI/np.absolute(poptLimGI)*100)
        print('Lin Fit (a,b):', poptLin)
        print('Lin Fit Recent (a,b):', poptLin_Recent)

        ## Model Comparison
        print()
        modelComparisonPreviewDays = np.array([*data["date"], *range(1,modelComparisonPreviewLength)])
        modelComparisonPreviewDates = [dates[-1] + timedelta(days=int(days_)) for days_ in modelComparisonPreviewDays]

        ## Model Comparison Plot
        plt.scatter(fitData[0], fitData[1], label='Vollständig Geimpfte Personen', marker='.', s=20)
        plt.scatter(boost_fitData[0], boost_fitData[1], label='Geboostete Personen', marker='.', s=20)
        plt.scatter(erst_fitData[0], erst_fitData[1], label='Erstgeimpfte Personen', marker='.', s=20)
        plot_model(funcSCurve, poptSCur, 'S-Curve ($y = a/(1 + e^{b - c \cdot x})$)')
        plt.ylabel('Personen in Mio')
        plt.grid(linestyle='--', alpha=0.5)
        plt.title('Sinnvolle Modelle')
        plt.xlabel('Tage ab dem ' + dates[-1].strftime('%d.%m.%Y'))
        plt.legend(prop={'size': 9})
        if saveFigure:
            figureFilename = folderName + 'Sinnvolle Modelle ' + dates[-1].strftime('%Y-%m-%d') + hindsightSuffix + '.png'
            plt.savefig(figureFilename)
            print('Saved Figure as: ' + figureFilename)
            if not isHindsight:
                figureFilename = 'Sinnvolle Modelle current.png'
                plt.savefig(figureFilename)
                print('Saved Figure as: ' + figureFilename)
            
        polyLines = plot_model(funcPolynomial, poptPoly, 'Poly. Fit ($y = a \cdot x^2 + b \cdot x + c$)')
        #plot_model_confidence(funcPolynomial, poptPoly, perrPoly, polyLines[0].get_color())            
        limGLines = plot_model(limitedGrowth, poptLimG, 'Beschränkte Impfrate ($y = a \cdot (x + d) + b/c \cdot e^{-ct} $)')
        #plot_model_confidence(limitedGrowth, poptLimG, perrLimG, limGLines[0].get_color(), np.sign(pcovLimG[0]))
        limGILines = plot_model(limitedGrowthIsrael, poptLimGI, 'Max. Impfr. wie Israel ($y = ' + str(round(maximaleImpfrateIsrael*Gesamtbevoelkerung, 2)) + ' \cdot (x + d) + b/c \cdot e^{-ct} $)')
        #plot_model_confidence(limitedGrowthIsrael, poptLimGI, perrLimGI, limGILines[0].get_color(), np.sign(pcovLimGI[0]))
        plot_model(funcCubic, poptCub, 'Cubic Fit ($y = a \cdot x^3 + b \cdot x^2 + c \cdot x + d$)')
        plot_model(funcLinear, poptLin_Recent, 'Aktuelle Impfrate ($y = a \cdot x + b$)')
        #plot_model(funcLinear, poptLin, 'Lin. Fit ($y = a \cdot x + b$)')

        plt.title('Vergleich verschiedener Modelle')#'Parabel-Modell (Polynom 2. Grad)')
        plt.legend(prop={'size': 7.5})
        if saveFigure:
            figureFilename = folderName + 'Vergleich mehrerer Modelle ' + dates[-1].strftime('%Y-%m-%d') + hindsightSuffix + '.png'
            plt.savefig(figureFilename)
            print('Saved Figure as: ' + figureFilename)
            if not isHindsight:
                figureFilename = 'Vergleich mehrerer Modelle current.png'
                plt.savefig(figureFilename)
                print('Saved Figure as: ' + figureFilename)
        if (showCurrentFigures and not isHindsight):
            plt.show()
        plt.close()


        ## Vorhersage
        print()
        estimationPreviewDays = np.array([*data["date"], *range(1,estimationPreviewLength)])
        estimationPreviewDates = [dates[-1] + timedelta(days=int(days_)) for days_ in estimationPreviewDays]

        '''
        estimate_params = [(limitedGrowthIsrael, poptLimGI)] #(funcPolynomial, poptPoly), (limitedGrowth, poptLimG), (funcLinear, poptLin_Recent)]

        
        indexOfHerdImmunity = list()
        dayOfHerdImmunity = list()
        dateOfHerdImmunity = list()
        dateStringsOfHerdImmunity = list()
        for function, params in estimate_params:
            index = np.argwhere(np.diff(np.sign(function(estimationPreviewDays, *params) - Gesamtbevoelkerung*impfQuote))).flatten()
            dayNr = estimationPreviewDays[index]
            date = dates[-1] + timedelta(days=int(dayNr))
            dateString = date.strftime('%d.%m.%Y')

            
            indexOfHerdImmunity.append(index)
            dayOfHerdImmunity.append(dayNr)
            dateOfHerdImmunity.append(date)
            dateStringsOfHerdImmunity.append(dateString)

        dayOfHerdImmunity = np.asarray(dayOfHerdImmunity)
        dateStringOfHerdImmunity = ', '.join(dateStringsOfHerdImmunity)
        print('Herden-Immunität ab:', dateStringOfHerdImmunity)
        if not isHindsight:
            HerdImmunityFilePath = "HerdImmunityDate.txt"
            with open(HerdImmunityFilePath, "w", encoding='utf-8') as outputFile:
                outputFile.write(dateStringOfHerdImmunity)
        '''
        
        ## Vorhersage Plot
        plt.figure(figsize=(10,4.3))
        plt.scatter(fitDates, fitData[1], label='Vollständig Geimpfte Personen', marker='.')
        plt.scatter(fitDates, boost_fitData[1], label='Geboostete Personen', marker='.')
        plt.scatter(fitDates, erst_fitData[1], label='Erstgeimpfte Personen', marker='.')

        polyLines = plot_estimate(funcPolynomial, poptPoly, 'Poly. Fit ($y = a \cdot x^2 + b \cdot x + c$)')
        limGLines = plot_estimate(limitedGrowth, poptLimG, 'Beschränkte Impfrate ($y = a \cdot (x + d) + b/c \cdot e^{-ct} $)')
        limGILines = plot_estimate(limitedGrowthIsrael, poptLimGI, 'Max. Impfr. wie Israel ($y = ' + str(round(maximaleImpfrateIsrael*Gesamtbevoelkerung, 2)) + ' \cdot (x + d) + b/c \cdot e^{-ct} $)')
        cubicLines = plot_estimate(funcCubic, poptCub, 'Cubic Fit ($y = a \cdot x^3 + b \cdot x^2 + c \cdot x + d$)')
        sCurLines = plot_estimate(funcSCurve, poptSCur, 'S-Curve ($y = a/(1 + e^{b - c \cdot x})$)')
        #plot_estimate_confidence(funcSCurve, funcSCurve_Ableitungen, poptSCur, pcovSCur, sCurLines[0].get_color())
        plot_estimate(funcLinear, poptLin_Recent, 'Aktuelle Impfrate ($y = a \cdot x + b$)', alpha=0.35)

        #plt.hlines(Gesamtbevoelkerung*impfQuote, estimationPreviewDates[0], estimationPreviewDates[-1], linestyle='dashed', label='Mögliche Herdenimmunität (' + str(int(impfQuote*100)) + '%)')
        plt.hlines(Gesamtbevoelkerung, estimationPreviewDates[0], estimationPreviewDates[-1], linestyle='dotted', label='Gesamt-Bevölkerung (100%)')
        #plt.scatter(dateOfHerdImmunity, [Gesamtbevoelkerung*impfQuote]*len(dateOfHerdImmunity), label= dateStringOfHerdImmunity, marker='o', color='red', s=9**2)

        plt.subplots_adjust(left=0.09, bottom=0.16, right=0.59, top=0.94)

        plt.xlabel('Datum')
        plt.ylabel('Personen in Mio')
        plt.xticks(rotation=18)
        plt.title('Vermutlich völlig inakkurate Vorhersage (Stand ' + fitDates[-1].strftime('%d.%m.%Y')+')')
        plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0., prop={'size': 9.5})
        plt.grid(linestyle='--', alpha=0.5)
        if saveFigure:
            figureFilename = folderName + 'Schlechte Impfvorhersage ' + dates[-1].strftime('%Y-%m-%d') + hindsightSuffix + '.png'
            plt.savefig(figureFilename)
            print('Saved Figure as: ' + figureFilename)
            if not isHindsight:
                figureFilename = 'Schlechte Impfvorhersage current.png'
                plt.savefig(figureFilename)
                print('Saved Figure as: ' + figureFilename)
        if (showCurrentFigures and not isHindsight):
            plt.show()
        plt.close()

        ## Tägliche Dosen Plot
        plt.figure(figsize=(10,4.3))
        plt.bar(fitDates[1:], np.diff(fitData[1]), width=1, label='Vollständige Impfungen pro Tag')
        plt.bar(fitDates[1:], np.diff(boost_fitData[1]), width=0.5, label='Boosts pro Tag')
        plot_estimate_diff(funcPolynomial, poptPoly, 'Poly. Fit ($y = a \cdot x^2 + b \cdot x + c$)')
        plot_estimate_diff(limitedGrowth, poptLimG, 'Beschränkte Impfrate ($y = a \cdot (x + d) + b/c \cdot e^{-ct} $)')
        plot_estimate_diff(limitedGrowthIsrael, poptLimGI, 'Max. Impfr. wie Israel ($y = ' + str(round(maximaleImpfrateIsrael*Gesamtbevoelkerung, 2)) + ' \cdot (x + d) + b/c \cdot e^{-ct} $)')
        #plot_estimate_diff(funcLinear, poptLin_Recent, 'Aktuelle Impfrate ($y = a \cdot x + b$)', alpha=0.35)
        plot_estimate_diff(funcCubic, poptCub, 'Cubic Fit ($y = a \cdot x^3 + b \cdot x^2 + c \cdot x + d$)')
        plot_estimate_diff(funcSCurve, poptSCur, 'S-Curve ($y = a/(1 + e^{b - c \cdot x})$)')

        plt.subplots_adjust(left=0.07, bottom=0.16, right=0.99, top=0.94)
        plt.xlabel('Datum')
        plt.ylabel('Pro Tag geimpfte Personen in Mio')
        plt.title('Tägliche Dosen mit Modellen (Stand ' + fitDates[-1].strftime('%d.%m.%Y')+')')
        plt.xticks(rotation=18)
        plt.legend(prop={'size': 8.5}) # bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0.)
        plt.grid(linestyle='--', alpha=0.5)
        if saveFigure:
            figureFilename = folderName + 'Schlechte Impfvorhersage pro Tag ' + dates[-1].strftime('%Y-%m-%d') + hindsightSuffix + '.png'
            plt.savefig(figureFilename)
            print('Saved Figure as: ' + figureFilename)
            if not isHindsight:
                figureFilename = 'Schlechte Impfvorhersage pro Tag current.png'
                plt.savefig(figureFilename)
                print('Saved Figure as: ' + figureFilename)
        if (showCurrentFigures and not isHindsight):
            plt.show()
        plt.close()



    if isHindsight:
        ## Animate
        def filename(plotName):
            return 'temp'+os.sep + plotName + dates[-1].strftime('%Y-%m-%d') + '*.png'
        def outputFilename(plotName):
            return plotName + 'current_hindsight.gif'
        def animate(plotName):
            img, *imgs = [Image.open(f) for f in sorted(glob.glob(filename(plotName)))]
            img.save(fp=outputFilename(plotName), format='GIF', append_images=[*imgs, *[imgs[-1]]*int(hindsight_Length/hindsight_Days_Step/10)], #append last image multiple times for a little break at the end of the loop
             save_all=True, duration=int(1000*13*hindsight_Days_Step/hindsight_Length), loop=0, optimize=True)
            print('Saved hindsight animation for', plotName)
        
        for name in ['Sinnvolle Modelle ', 'Vergleich mehrerer Modelle ', 'Schlechte Impfvorhersage ', 'Schlechte Impfvorhersage pro Tag ']:
            animate(name)
        clearTempFolder()
